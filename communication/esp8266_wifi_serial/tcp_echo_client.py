'''
Tests roundtrip

Serial console -> Wifi -> Computer -> Wifi -> Serial console

Usage:
* Start Wifi AP
* Start ESP8266 connected to computer
* Connect computer to AP
* Start Python program
* Use serial program to access serial interface of ESP8266
* Type message, wait for echo
'''

import socket

server_name = '192.168.0.101'
server_port = 8000

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

while True:
    print(f'Connecting to {server_name} port {server_port} ...')
    sock.connect((server_name, server_port))
    try:
        while True:
            data = sock.recv(4096)
            print(data)
            sock.sendall(data)
    finally:
        print('Closing socket ...')
        sock.close()

