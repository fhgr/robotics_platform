#include <ESP8266WiFi.h>

const long serial_baud = 115200;
const char *wifi_ssid = "TP-Link_F186";
const char *wifi_pass = "62258579";
const int server_port = 8000;

WiFiServer server(server_port); // TCP/IP server

void setup() {
  // Setup serial connection
  Serial.begin(serial_baud);
  // Connect to WIFI
  WiFi.begin(wifi_ssid, wifi_pass);
  for (int i = 0; ; i++) {
    if (WiFi.status() == WL_CONNECTED) {
      break; // Success, we are connected
    }
    if (i >= 60) { // Error: Too many attempts to connect
      while (true) {
        Serial.print("Unable to connect to WIFI \"");
        Serial.print(wifi_ssid);
        Serial.println("\".");
        delay(1000);
      }
    }
    delay(1000);
  }
  // Start TCP server
  server.begin();
}

void loop() {
  WiFiClient client = server.available();
  if (client) {
    while (client.connected()) {
      // While there is TCP data to read, sent it to serial
      if (client.available() > 0) {
        Serial.write(client.read());
      }
      // While there is serial data to read, sent it to TCP
      if (Serial.available() > 0) {
        client.write(Serial.read());
      }
    }
    client.stop();
  }
}
