'''
Tests roundtrip

Computer -> Wifi -> Serial console -> Serial console -> Wifi -> Computer

Usage:
* Connect RX and TX pins of ESP8266
* Start Wifi AP
* Start ESP8266
* Connect computer to AP
* Start Python program
'''

import socket
import time

server_name = '192.168.0.100'
server_port = 8000

def ping(sock, send_str):
    # Send data
    send_buffer = send_str.encode('utf-8')
    tic = time.time()
    sock.sendall(send_buffer)
    # Receive data
    amount_expected = len(send_buffer)
    recv_buffer = bytearray()
    while len(recv_buffer) < amount_expected:
        data = sock.recv(amount_expected - len(recv_buffer))
        recv_buffer += data
    toc = time.time()
    recv_str = recv_buffer.decode('utf-8')
    if send_str != recv_str:
        raise Exception(f'Ping error: {send_str} != {recv_str}')
    return toc - tic

    

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Connecting ...')
sock.connect((server_name, server_port))
try:
    for i in range(10):
        print(f'Ping {i+1}:')
        sec = ping(sock, 'ping')
        print(f'    Roundtrip took {1000*sec:.0f} ms')
finally:
    print('Disconnecting ...')
    sock.close()

