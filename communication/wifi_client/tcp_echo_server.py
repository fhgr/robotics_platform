import signal
import socket
import sys



# Config
server_port = 8000
block_size_bytes = 16



# Set up CTRL-C handler
def signal_handler(sig, frame):
	print('Closing server socket ...')
	sock.close()
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
print(f'Listening to port {server_port}')
sock.bind(('', server_port))

# Listen for incoming connections, queue lenght 1
sock.listen(1)

while True:
	# Wait for a connection
	print('Waiting for a connection ...')
	connection, client_address = sock.accept()
	try:
		print(f'Connection from {client_address}')

		# Receive the data in small chunks and retransmit it
		while True:
			data = connection.recv(block_size_bytes)
			if data:
				connection.sendall(data)
			else:
				print('Closing client connection')
				break

	finally:
		# Clean up the connection
		connection.close()
