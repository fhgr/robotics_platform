import os
import sys
from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtNetwork import QHostAddress, QTcpSocket

class Ui(QtWidgets.QMainWindow):

    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi('wifi_client.ui', self)
        # Network
        self.lineEditConnect.setText("127.0.0.1:8000")
        self.socket = QTcpSocket()
        self.lineEditLog.setText("log.csv")
        self.file = None
        # Connect signals/slots: Networking
        self.pushButtonConnect.clicked.connect(self.pushButtonConnect_clicked)
        self.socket.connected.connect(self.socket_connected)
        self.socket.disconnected.connect(self.socket_disconnected)
        self.socket.error.connect(self.socket_error)
        self.socket.readyRead.connect(self.socket_readyRead)
        # Connect signals/slots: Number buttons
        self.pushButton1.pressed.connect(self.pushButton1_pressed)
        self.pushButton1.released.connect(self.pushButton1_released)
        self.pushButton2.pressed.connect(self.pushButton2_pressed)
        self.pushButton2.released.connect(self.pushButton2_released)
        self.pushButton3.pressed.connect(self.pushButton3_pressed)
        self.pushButton3.released.connect(self.pushButton3_released)
        self.pushButton4.pressed.connect(self.pushButton4_pressed)
        self.pushButton4.released.connect(self.pushButton4_released)
        self.pushButtonArrowLeft.pressed.connect(self.pushButtonArrowLeft_pressed)
        self.pushButtonArrowLeft.released.connect(self.pushButtonArrowLeft_released)
        self.pushButtonArrowRight.pressed.connect(self.pushButtonArrowRight_pressed)
        self.pushButtonArrowRight.released.connect(self.pushButtonArrowRight_released)
        self.pushButtonArrowUp.pressed.connect(self.pushButtonArrowUp_pressed)
        self.pushButtonArrowUp.released.connect(self.pushButtonArrowUp_released)
        self.pushButtonArrowDown.pressed.connect(self.pushButtonArrowDown_pressed)
        self.pushButtonArrowDown.released.connect(self.pushButtonArrowDown_released)
        # Connect signals/slots: Log to file
        self.pushButtonLog.clicked.connect(self.pushButtonLog_clicked)

    @pyqtSlot()
    def pushButtonConnect_clicked(self):
        if not self.socket.isOpen():
            param = self.lineEditConnect.text().split(':')
            if len(param) != 2:
                return
            self.socket.connectToHost(QHostAddress(param[0]), int(param[1]))
        else:
            self.socket.close()

    @pyqtSlot()
    def socket_connected(self):
        self.lineEditConnect.setEnabled(False)
        self.pushButtonConnect.setText('Disconnect')

    @pyqtSlot()
    def socket_disconnected(self):
        self.lineEditConnect.setEnabled(True)
        self.pushButtonConnect.setText('Connect')

    @pyqtSlot()
    def socket_error(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Warning)
        msg.setText(self.socket.errorString())
        msg.setWindowTitle('Network error')
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()

    @pyqtSlot()
    def socket_readyRead(self):
        data = self.socket.readAll()
        if self.file is not None:
            data_str = str(data.data(), encoding='utf-8')
            if not data_str.endswith('\n'):
                data_str += '\n'
            self.file.write(data_str)

    def __controller_send(self, cmd):
        cmd_bytes = cmd.encode('utf-8')
        # Calculate checksum
        sum = 0
        for c in cmd_bytes:
            sum += c
        crc = (~sum & 0xff).to_bytes(1, byteorder='little')
        cmd_bytes += crc
        # Send if socket available
        if self.socket.isOpen():
            self.socket.write(cmd_bytes)

    @pyqtSlot()
    def pushButton1_pressed(self):
        self.__controller_send('!B11')

    @pyqtSlot()
    def pushButton1_released(self):
        self.__controller_send('!B10')

    @pyqtSlot()
    def pushButton2_pressed(self):
        self.__controller_send('!B21')

    @pyqtSlot()
    def pushButton2_released(self):
        self.__controller_send('!B20')

    @pyqtSlot()
    def pushButton3_pressed(self):
        self.__controller_send('!B31')

    @pyqtSlot()
    def pushButton3_released(self):
        self.__controller_send('!B30')

    @pyqtSlot()
    def pushButton4_pressed(self):
        self.__controller_send('!B41')

    @pyqtSlot()
    def pushButton4_released(self):
        self.__controller_send('!B40')

    @pyqtSlot()
    def pushButtonArrowLeft_pressed(self):
        self.__controller_send('!B51')

    @pyqtSlot()
    def pushButtonArrowLeft_released(self):
        self.__controller_send('!B50')

    @pyqtSlot()
    def pushButtonArrowRight_pressed(self):
        self.__controller_send('!B61')

    @pyqtSlot()
    def pushButtonArrowRight_released(self):
        self.__controller_send('!B60')

    @pyqtSlot()
    def pushButtonArrowUp_pressed(self):
        self.__controller_send('!B71')

    @pyqtSlot()
    def pushButtonArrowUp_released(self):
        self.__controller_send('!B70')

    @pyqtSlot()
    def pushButtonArrowDown_pressed(self):
        self.__controller_send('!B81')

    @pyqtSlot()
    def pushButtonArrowDown_released(self):
        self.__controller_send('!B80')

    @pyqtSlot()
    def pushButtonLog_clicked(self):
        if self.file is None:
            try:
                self.file = open(self.lineEditLog.text(), 'w')
                self.lineEditLog.setEnabled(False)
                self.pushButtonLog.setText('Disable')
            except Exception as ex:
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Warning)
                msg.setText(str(ex))
                msg.setWindowTitle('File error')
                msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
                msg.exec_()
        else:
            self.file.close()
            self.file = None
            self.lineEditLog.setEnabled(True)
            self.pushButtonLog.setText('Enable')



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = Ui()
    window.setWindowTitle(os.path.basename(sys.argv[0]))
    window.show()
    app.exec_()
